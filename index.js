const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});


(async () => {
    const db = require("./db"); 
    const chat = await db.chatHistorico();
    console.log(chat);
})();


(async () => {
    const db = require("./db");
    
    io.on('connection', (socket) => {
      socket.on('chat message', mensagem => {
        io.emit('chat message', mensagem);
        const result =  db.inserirConversa({ msg: mensagem });
      });
      });

    console.log('SELECT * FROM tb_chat');
    const clientes = await db.chatHistorico();
    console.log(clientes);
})();

http.listen(port, () => {
  console.log(`Socket.IO server running at http://localhost:${port}/`);
});
