async function connect(){
    if(global.connection && global.connection.state !== 'disconnected')
        return global.connection;
 
    const mysql = require("mysql2/promise");
    const connection = await mysql.createConnection("mysql://root:abc123@localhost:3306/chat");
    global.connection = connection;
    return connection;
}

async function chatHistorico(){
    const conn = await connect();
    const [rows] = await conn.query('SELECT * FROM tb_chat;');
    return rows;
}
 

async function inserirConversa(chat){
    const conn = await connect();
    const sql = 'INSERT INTO tb_chat(msg) VALUES (?);';
    const values = [ chat.msg];
    return await conn.query(sql, values);
}
module.exports = {chatHistorico, inserirConversa}